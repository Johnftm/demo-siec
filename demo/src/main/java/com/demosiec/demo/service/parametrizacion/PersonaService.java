package com.demosiec.demo.service.parametrizacion;

import org.springframework.stereotype.Service;

import com.demosiec.demo.parametrizacion.Persona;

@Service
public class PersonaService {

	public void validarMayoresEdad(Persona persona) throws PersonaException {
		if (persona.getEdad() > 18)
			return;

		throw new PersonaException("ser mayor de 18 años", persona.getNombre());
	}

}
