package com.demosiec.demo.service.parametrizacion;

public class PersonaException extends Exception {

	private static final long serialVersionUID = 1L;

	private String nombre;

	public PersonaException(String error, String nombrePersona) {
		super(nombrePersona + " Debe " + error);
	}

	public String getNombre() {
		return nombre;
	}
}
