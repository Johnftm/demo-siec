package com.demosiec.demo.repository.inventario;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.demosiec.demo.inventario.Categoria;
import com.demosiec.demo.inventario.Producto;

public interface ProductoRepository extends CrudRepository<Producto, Long> {

	List<Producto> findByCategoria(Categoria categoria);

	// HQL
	@Query(value = "SELECT p FROM com.demosiec.demo.inventario.Producto p JOIN p.categoria c WHERE c.estado = :estadoFiltro ")
	List<Producto> filtrarPorCategoriaConEstado(@Param("estadoFiltro") String estado);

	// NATIVA
	@Query(value = "select * from producto p inner join categoria c on p.categoria=c.id where c.estado=:estadoFiltro", nativeQuery = true)
	List<Producto> filtrarPorCategoriaConEstadoNat(@Param("estadoFiltro") String estado);
	
	// Optimizacion - HQL
	/**
	 * @see Producto#Producto(String, Integer, Float)
	 */
	@Query(value = "SELECT NEW com.demosiec.demo.inventario.Producto(p.nombre, p.cantidad, p.precio) FROM com.demosiec.demo.inventario.Producto p JOIN p.categoria c WHERE c.estado = :estadoFiltro ")
	List<Producto> filtrarPorCategoriaConEstadoParaVista(@Param("estadoFiltro") String estado);

}
