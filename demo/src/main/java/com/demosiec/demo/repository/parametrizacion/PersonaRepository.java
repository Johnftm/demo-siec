package com.demosiec.demo.repository.parametrizacion;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.demosiec.demo.parametrizacion.Persona;

public interface PersonaRepository extends CrudRepository<Persona, Long> {

	@Query("SELECT p FROM com.demosiec.demo.parametrizacion.Persona p WHERE p.documento = :documento")
	Persona filtrarPorDocumento(@Param("documento") Integer documento);
	
	List<Persona> findByDocumento(Integer documento);

	List<Persona> findByDocumentoAndNombre(Integer documento, String nombre);

	List<Persona> findByEdadGreaterThan(Integer edad);
}
