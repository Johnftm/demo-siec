package com.demosiec.demo.repository.inventario;

import org.springframework.data.repository.CrudRepository;

import com.demosiec.demo.inventario.Categoria;

public interface CategoriaRepository extends CrudRepository<Categoria, Long>{

}
