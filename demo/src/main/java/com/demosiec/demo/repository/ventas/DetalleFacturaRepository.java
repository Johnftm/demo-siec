package com.demosiec.demo.repository.ventas;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.demosiec.demo.ventas.DetalleFactura;

public interface DetalleFacturaRepository extends CrudRepository<DetalleFactura, Long> {

	/**
	 * @see DetalleFactura#DetalleFactura(String, Float, Integer, java.util.Date,
	 *      String)
	 */
	@Query("SELECT NEW com.demosiec.demo.ventas.DetalleFactura(p.nombre, p.precio, d.cantidad, d.fechaCompra, c.nombre) " //
			+ "FROM com.demosiec.demo.ventas.DetalleFactura d " //
			+ "JOIN d.producto p JOIN p.categoria c " //
			+ "ORDER BY d.fechaCompra ASC")
	Iterable<DetalleFactura> consulta();

	// ---- Paginación para tablas | GRAN PARTE DE SIEC
	
	@Query("SELECT d FROM com.demosiec.demo.ventas.DetalleFactura d WHERE (:cantidad IS NULL OR d.cantidad = :cantidad) ")
	List<DetalleFactura> slice(@Param("cantidad") Integer cantidad, Pageable page);

	@Query("SELECT COUNT(d) FROM com.demosiec.demo.ventas.DetalleFactura d WHERE (:cantidad IS NULL OR d.cantidad = :cantidad)")
	long count(@Param("cantidad") Integer cantidad);

	// ---- Paginación para tablas | Mejor practica
	
	@Query("SELECT d FROM com.demosiec.demo.ventas.DetalleFactura d WHERE (:cantidad IS NULL OR d.cantidad = :cantidad)")
	Page<DetalleFactura> slicePage(@Param("cantidad") Integer cantidad, Pageable page);
}
