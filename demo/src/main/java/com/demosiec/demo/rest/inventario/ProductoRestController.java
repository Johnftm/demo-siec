package com.demosiec.demo.rest.inventario;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demosiec.demo.inventario.Categoria;
import com.demosiec.demo.inventario.Producto;
import com.demosiec.demo.repository.inventario.ProductoRepository;

@RestController
@RequestMapping(value = "inventario/producto")
public class ProductoRestController {

	private ProductoRepository productoRepository;

	public ProductoRestController(ProductoRepository productoRepository) {
		super();
		this.productoRepository = productoRepository;
	}

	@GetMapping
	public Iterable<Producto> getAll() {
		return productoRepository.findAll();
	}
	
	@GetMapping("filtro")
	public List<Producto> filtrarPorCategoria(@RequestParam Categoria categoria) {
		return productoRepository.findByCategoria(categoria);
	}
	
	@GetMapping("activas")
	public List<Producto> filtrarPorCategoriasActivas() {
		return productoRepository.filtrarPorCategoriaConEstado(Categoria.ACTIVA);
	}
	
	@GetMapping("inactivas")
	public List<Producto> filtrarPorCategoriasInactivas() {
		return productoRepository.filtrarPorCategoriaConEstadoNat(Categoria.INACTIVA);
	}
	
	@GetMapping("vista")
	public List<Producto> filtrarPorCategoriaConEstadoParaVista() {
		return productoRepository.filtrarPorCategoriaConEstadoParaVista(Categoria.INACTIVA);
	}
	
	@PostMapping
	public Producto save(@RequestBody Producto producto) {
		return this.productoRepository.save(producto);
	}

}
