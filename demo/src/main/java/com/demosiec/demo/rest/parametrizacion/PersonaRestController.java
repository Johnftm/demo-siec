package com.demosiec.demo.rest.parametrizacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demosiec.demo.parametrizacion.Persona;
import com.demosiec.demo.repository.parametrizacion.PersonaRepository;
import com.demosiec.demo.service.parametrizacion.PersonaException;
import com.demosiec.demo.service.parametrizacion.PersonaService;

@RestController
@RequestMapping(value = "parametrizacion/persona")
public class PersonaRestController {

	private PersonaRepository personaRepository;

	private PersonaService personaService;

	// Injection dependences
	@Autowired
	public PersonaRestController(PersonaRepository personaRepository, PersonaService personaService) {
		super();
		this.personaRepository = personaRepository;
		this.personaService = personaService;
	}

	// ------------- GET ----------------------

	@GetMapping
	public Iterable<Persona> getAll() {
		return this.personaRepository.findAll();
	}

	// ------------- @PathVariable ----------------------

	@GetMapping("/{documento}")
	public List<Persona> filtrarPorDocumento(@PathVariable("documento") Integer documento) {
		// return this.personaRepository.filtrarPorDocumento(documento);
		return this.personaRepository.findByDocumento(documento);
	}

	@GetMapping("/{documento}/{nombre}")
	public List<Persona> filtrarPorDocumento(@PathVariable("documento") Integer documento,
			@PathVariable("nombre") String nombre) {
		return this.personaRepository.findByDocumentoAndNombre(documento, nombre);
	}

	@GetMapping("edad/{edad}")
	public List<Persona> filtrarPorEdadMayorQue(@PathVariable("edad") Integer edad) {
		return this.personaRepository.findByEdadGreaterThan(edad);
	}

	// ------------- @RequestParam ----------------------

	// http://localhost:8080/parametrizacion/persona/filtro?documento=12345&isTrue=false&nombre=john
	@GetMapping("filtro")
	public List<Persona> filtrarPorDocumento2(
			@RequestParam(name = "documento", required = false, defaultValue = "123") int documento,
			@RequestParam(name = "nombre", required = false) String nombre,
			@RequestParam(name = "isTrue", required = false, defaultValue = "true") Boolean isTrue) {

		System.out.println("documento: " + documento);
		System.out.println("nombre: " + nombre);
		System.out.println("isTrue: " + isTrue);

		return this.personaRepository.findByDocumento(documento);
	}

	// ------------- POST ----------------------

	@PostMapping
	public ResponseEntity<String> crearPersona(@RequestBody Persona persona) {
		if (persona.getId() != null && this.personaRepository.existsById(persona.getId()))
			return ResponseEntity.badRequest().body("La persona ya existe en DB");

		try {
			this.personaService.validarMayoresEdad(persona);
		} catch (PersonaException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
		
		this.personaRepository.save(persona);
		return ResponseEntity.ok("Persona creada correctamente");
	}

	// ------------- PUT ----------------------

	@PutMapping
	public ResponseEntity<String> actualizarPersona(@RequestBody Persona persona) {
		if (this.personaRepository.existsById(persona.getId())) {
			this.personaRepository.save(persona);
			return ResponseEntity.ok("Persona actualizada correctamente");
		} else
			return ResponseEntity.badRequest().body("La persona no existe en DB");
	}

	// ------------- DELETE ----------------------

	@DeleteMapping
	public ResponseEntity<String> eliminarPersona(@RequestParam Persona persona) {
		this.personaRepository.delete(persona);
		return ResponseEntity.badRequest().body("La persona fue eliminada correctamente");
	}

}
