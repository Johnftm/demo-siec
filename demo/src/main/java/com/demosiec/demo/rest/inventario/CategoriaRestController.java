package com.demosiec.demo.rest.inventario;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demosiec.demo.inventario.Categoria;
import com.demosiec.demo.repository.inventario.CategoriaRepository;

@RestController
@RequestMapping(value = "inventario/categoria")
public class CategoriaRestController {

	private CategoriaRepository categoriaRepository;

	public CategoriaRestController(CategoriaRepository categoriaRepository) {
		super();
		this.categoriaRepository = categoriaRepository;
	}

	@GetMapping
	public Iterable<Categoria> getAll() {
		return categoriaRepository.findAll();
	}
	
	@PostMapping
	public Categoria save(@RequestBody Categoria categoria) {
		return this.categoriaRepository.save(categoria);
	}
}
