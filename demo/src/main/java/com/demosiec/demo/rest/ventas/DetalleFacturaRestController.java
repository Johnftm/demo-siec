package com.demosiec.demo.rest.ventas;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demosiec.demo.repository.ventas.DetalleFacturaRepository;
import com.demosiec.demo.utils.Utils;
import com.demosiec.demo.ventas.DetalleFactura;

@RestController
@RequestMapping(value = "ventas/detallefactura")
public class DetalleFacturaRestController {

	private DetalleFacturaRepository detalleFacturaRepository;

	@Autowired
	public DetalleFacturaRestController(DetalleFacturaRepository detalleFacturaRepository) {
		this.detalleFacturaRepository = detalleFacturaRepository;
	}

	@GetMapping
	public Iterable<DetalleFactura> getAll() {
		return detalleFacturaRepository.findAll();
	}

	@GetMapping("consulta")
	public Iterable<DetalleFactura> consulta() {
		return detalleFacturaRepository.consulta();
	}

	// ---- Paginación para tablas | GRAN PARTE DE SIEC

	@RequestMapping(method = RequestMethod.GET, value = "slice")
	public Iterable<DetalleFactura> slice(@RequestParam(value = "offset", defaultValue = "0") Integer offset,
			@RequestParam(value = "rows", defaultValue = "10") Integer rows,
			@RequestParam(required = false) Integer cantidad) {

		PageRequest page = Utils.createPage(offset, rows);

		return detalleFacturaRepository.slice(cantidad, page);
	}

	@RequestMapping(method = RequestMethod.GET, value = "count")
	public Long count(@RequestParam(required = false) Integer cantidad) {
		return detalleFacturaRepository.count(cantidad);
	}

	// ---- Paginación para tablas | Mejor practica

	@RequestMapping(method = RequestMethod.GET, value = "slice-page")
	public Page<DetalleFactura> slicePage(@RequestParam(value = "offset", defaultValue = "0") Integer offset,
			@RequestParam(value = "rows", defaultValue = "10") Integer rows,
			@RequestParam(required = false) Integer cantidad) {

		PageRequest page = Utils.createPage(offset, rows);

		return detalleFacturaRepository.slicePage(cantidad, page);
	}
}
