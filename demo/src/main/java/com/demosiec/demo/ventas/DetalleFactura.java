package com.demosiec.demo.ventas;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;

import com.demosiec.demo.inventario.Categoria;
import com.demosiec.demo.inventario.Producto;
import com.demosiec.demo.repository.ventas.DetalleFacturaRepository;

@Entity
public class DetalleFactura {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "producto")
	private Producto producto;

	@Column(nullable = false)
	@Min(0)
	private Integer cantidad;

	@Column(nullable = false)
	private Date fechaCompra;

	/**
	 * @see DetalleFacturaRepository#consulta()
	 */
	public DetalleFactura(String nombreProducto, Float precioProducto, @Min(0) Integer cantidad, Date fechaCompra,
			String nombreCategoria) {

		this.producto = new Producto();

		this.producto.setPrecio(precioProducto);
		Categoria categoria = new Categoria();
		categoria.setNombre(nombreCategoria);

		this.producto.setCategoria(categoria);

		this.producto.setNombre(nombreProducto);
		this.cantidad = cantidad;
		this.fechaCompra = fechaCompra;
	}

	public Float getCosto() {
		return this.cantidad * this.producto.getPrecio();
	}

	public DetalleFactura() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Date getFechaCompra() {
		return fechaCompra;
	}

	public void setFechaCompra(Date fechaCompra) {
		this.fechaCompra = fechaCompra;
	}
}
