package com.demosiec.demo.inventario;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;

import com.demosiec.demo.repository.inventario.ProductoRepository;

@Entity
public class Producto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false, length = 64)
	private String nombre;

	@Column(nullable = false)
	@Min(0)
	private Integer cantidad;

	@Column(nullable = false)
	@Min(0)
	private Float precio;

	@ManyToOne(fetch = FetchType.LAZY) //
	@JoinColumn(name = "categoria", nullable = false)
	private Categoria categoria;

	public Producto() {
	}

	/**
	 * @see ProductoRepository#filtrarPorCategoriaConEstadoParaVista(String)
	 */
	public Producto(String nombre, @Min(0) Integer cantidad, @Min(0) Float precio) {
		this.nombre = nombre;
		this.cantidad = cantidad;
		this.precio = precio;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Float getPrecio() {
		return precio;
	}

	public void setPrecio(Float precio) {
		this.precio = precio;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
}
