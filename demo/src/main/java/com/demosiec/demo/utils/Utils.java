package com.demosiec.demo.utils;

import org.springframework.data.domain.PageRequest;

public class Utils {

	public static PageRequest createPage(int offset, int rows) {
		if (rows == 0)
			rows = 10;
		int page = offset / rows;
		return PageRequest.of(page, rows);
	}
}
