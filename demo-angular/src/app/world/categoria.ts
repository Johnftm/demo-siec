export interface Categoria {
    id?: number
    nombre?: string
    estado?: 'A' | 'I'
}