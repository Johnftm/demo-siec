import { Producto } from "./producto";

export interface DetalleFactura {
    id: number
    cantidad: number
    fechaCompra: Date
    producto: Producto
}