import { Categoria } from "./categoria";

export interface Producto {
    id: number
    cantidad: number
    nombre: string
    precio: number
    categoria: Categoria
}