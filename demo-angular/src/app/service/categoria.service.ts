import { Injectable } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { Categoria } from '../world/categoria';
import { Links } from './link.service';
import { ServiwRest } from './serviw-rest.service';

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {

  constructor(
    private links: Links,
    private serviwRest: ServiwRest
  ) { }

  getAll() {
    const promesa = this.serviwRest.get(this.links.categoria)
    return promesa;
  } 
  getOptions(){
    const callPromise =(resolve,reject)=>{
      const callOk = (list:Categoria[])=>resolve(this.refactorOptions(list))
      const callError =(error)=>reject(error)
      const promesa = this.getAll()
      promesa.then(callOk, callError)
    }
    return new Promise(callPromise)
  }

  refactorOptions(list:Categoria[]):SelectItem[]{
    const options:SelectItem[]=[]
    for (let i = 0; i < list.length; i++) {
      const element = list[i];
      const option:SelectItem={
        label:element.nombre,
        value:element
      }
      options.push(option)
    }
    return options
  }

  save(categoria:Categoria){
    const promesa = this.serviwRest.post(this.links.categoria, categoria)
    return promesa;
  }
}
