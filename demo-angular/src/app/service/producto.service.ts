import { Injectable } from '@angular/core';
import { Producto } from '../world/producto';
import { Links } from './link.service';
import { ServiwRest } from './serviw-rest.service';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  constructor(
    private links: Links,
    private serviwRest: ServiwRest
  ) { }
  getAll() {
    const promesa = this.serviwRest.get(this.links.producto)
    return promesa;
  }
  save(producto: Producto) {
    const promesa = this.serviwRest.post(this.links.producto, producto)
    return promesa;
  }
}
