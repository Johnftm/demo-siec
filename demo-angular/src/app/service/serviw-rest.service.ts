import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { isNumber } from 'util';

@Injectable({
  providedIn: 'root'
})
export class ServiwRest {

  constructor(
    public http: HttpClient
  ) { }

  // GET
  public get(link: string, content = {}, refactor = undefined) {
    const func = (resolve: (value: any) => void, reject: (value: any) => void) => {
      const dataOk = (val: any) => {
        let data: any = {};
        try {
          data = val;
          if (refactor) {
            data = refactor(data);
          }
        } catch (e) { }
        resolve(data);
      }
      const dataError = (error: Response) => reject(error);
      const linkParams: string = this.getLinkParameters(content);
      link += linkParams;

      const dataCall = this.http.get(link);
      dataCall.subscribe(dataOk, dataError);
    }

    const promise: Promise<any> = new Promise<any>(func);
    return promise;
  }

  // POST
  public post(link: string, content: any, params = {}, refactor = undefined): Promise<any> {
    // 1.
    const options = this.getOptionsArg();
    const func: (resolve, reject) => void = (resolve, reject) => {
      // 1.4.1.
      const dataOk: (val: any) => void = (val: any) => {
        // 1.4.1.1.1.
        let data: any = {};
        try {
          data = val;
          if (refactor) {
            data = refactor(data);
          }
        } catch (e) { }
        resolve(data);
      };
      const dataError: (error: Response) => void = (error: Response) => {
        // 1.4.1.2.1
        reject(error);
      };
      // 1.4.2.
      const linkParams: string = this.getLinkParameters(params);
      link += linkParams;
      // 1.4.3
      const dataSend: string = JSON.stringify(content);
      const dataCall = this.http.post(link, dataSend, options);
      dataCall.subscribe(dataOk, dataError);
    };
    // 2.
    const promise: Promise<any> = new Promise<any>(func);

    return promise;
  }

  // PUT
  public put(link: string, content: any, params = {}, refactor = undefined): Promise<any> {
    // 1.
    const func: (resolve, reject) => void = (resolve, reject) => {
      // 1.4.1.
      const dataOk: (val: any) => void = (val: any) => {
        // 1.4.1.1.1.
        let data: any = {};
        try {
          data = val;
          if (refactor) {
            data = refactor(data);
          }
        } catch (e) { }
        resolve(data);
      };
      const dataError: (error: Response) => void = (error: Response) => {
        // 1.4.1.2.1
        reject(error);
      };
      // 1.4.2.
      const linkParams: string = this.getLinkParameters(params);
      link += linkParams;
      // 1.4.3.
      const dataSend: string = JSON.stringify(content);
      const dataCall = this.http.put(link, dataSend);
      dataCall.subscribe(dataOk, dataError);
    };
    // 2.
    const promise: Promise<any> = new Promise<any>(func);

    return promise;
  }

  public getLinkParameters(data: any): string {
    // 1.
    let query = false;
    let url = '';

    for (const x in data) {
      if (data.hasOwnProperty(x) && (isNumber(data[x]) || (undefined !== data[x] && null !== data[x]))) {
        if (!query) {
          // 2.
          url += '?';
          query = true;
        } else {
          // 3.
          url += '&';
        }
        // 4.

        url += x + '=' + data[x];
      }
    }

    return url;
  }

  public getOptionsArg(): any {
    // 1.
    const header: { [name: string]: any } = {
        'Content-Type': 'application/json'
    };
    // 2.
    const headers = new HttpHeaders(header);
    // 3.
    const options: any = {
        headers: headers,
        // withCredentials: true
    };

    return options;
}
}
