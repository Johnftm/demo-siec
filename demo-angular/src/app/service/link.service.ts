import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class Links {

  private readonly home: string;

  constructor() {
    if (window.location.port == '4200') {
      this.home = 'http://localhost:8080';
      //this.home = 'http://10.1.1.245:8080';
    } else {
      this.home = '';
    }
  }

  get categoria() {
    return this.home + '/inventario/categoria'
  }
  get producto(){
    return this.home + '/inventario/producto'
  }
}
