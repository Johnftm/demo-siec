import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'activoInactivo'
})
export class ActivoInactivoPipe implements PipeTransform {

  transform(value: string): any {
    switch (value) {
      case 'A': return 'Activo'
      case 'I': return 'Inactivo'
      default: return value;
    }
  }

}
