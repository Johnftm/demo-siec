import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { DialogService } from 'primeng/api';
import { SelectItem } from 'primeng/components/common/selectitem';
import { ProductoService } from 'src/app/service/producto.service';
import { Producto } from 'src/app/world/producto';
import { ProductoFormComponent } from './producto-form.component';

@Component({
  selector: 'app-producto-table',
  templateUrl: './producto-table.component.html'
})
export class ProductoTableComponent implements OnInit {

  productos: Producto[];

  categorias: SelectItem[];

  cols: any[];

  constructor(
    private productoService:  ProductoService,
    private dialogService: DialogService

  ) { }

  ngOnInit() {
    this.getAll();
    // this.categorias = [
    //   { label: 'Todos', value: null },
    //   { label: 'Lacteos', value: { id: 2, nombre: 'Lacteos', estado: 'I' } },
    //   { label: 'Otros', value: { id: 1, nombre: 'Otros', estado: 'I' } }
    // ];

    // this.productos = [
    //   {
    //     id: 1,
    //     cantidad: 10,
    //     nombre: 'Leche Bolsa',
    //     precio: 500,
    //     categoria: { id: 2, nombre: 'Lacteos', estado: 'I' }
    //   }
    // ]

    this.cols = [
      { field: 'nombre', header: 'Nombre' },
      { field: 'cantidad', header: 'Cantidad' },
      { field: 'precio', header: 'Precio' },
      { field: 'categoria', header: 'Categoría' },
    ];
  }

  verFormulario(){
    const ref = this.dialogService.open(ProductoFormComponent,{
      header:'Formulario Producto',
      width:'40%',
      data:{}
    })
    ref.onClose.subscribe(producto=>this.productos.push(producto))
  }

  getAll(){
    const getOk = (list: Producto[]) => {
      this.productos = list;
    }
    const getError = (error) => {
      console.log(error);
    }
    const promesa = this.productoService.getAll();
    promesa.then(getOk, getError)
  }

}
