import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DynamicDialogRef, SelectItem } from 'primeng/api';
import { CategoriaService } from 'src/app/service/categoria.service';
import { ProductoService } from 'src/app/service/producto.service';
import { Categoria } from 'src/app/world/categoria';
import { Producto } from 'src/app/world/producto';

@Component({
  selector: 'app-producto-form',
  templateUrl: './producto-form.component.html',
  styles: []
})
export class ProductoFormComponent implements OnInit {

  public productoForm: FormGroup
  public categorias: SelectItem[]
  constructor(
    private formBuilder: FormBuilder,
    private categoriaService: CategoriaService,
    private productoService: ProductoService,
    private ref: DynamicDialogRef
  ) { }

  ngOnInit() {
    this.productoForm = this.formBuilder.group({
      id:[
        null,
        Validators.compose([
      
        ])
      ],
      cantidad:[
        null,
        Validators.compose([
          Validators.required,
          Validators.min(0)
        ])
      ],
      nombre:[
        null,
        Validators.compose([
          Validators.required,
          Validators.maxLength(64)
        ])
      ],
      precio:[
        null,
        Validators.compose([
          Validators.required,
          Validators.min(0)
        ])
      ],
      categoria:[
        null,
        Validators.compose([
        ])
      ],
    });
    const getCategorias =(options:SelectItem[])=>{
      this.categorias=options
    }
    const getCategoriasError =(error)=>{
      console.error(error);
    }

    const promesa = this.categoriaService.getOptions()
    promesa.then(getCategorias,getCategoriasError)

  }

  public save(){
    const saveOk =(producto: Producto)=>{
      console.log(producto)
      this.ref.close(producto)
    }
    const saveError =(error)=>{
      console.log(error)
    }
    const promise = this.productoService.save(this.productoForm.value)
    promise.then(saveOk,saveError)
   }

}
