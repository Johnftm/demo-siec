import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ServiwParams } from 'src/app/service/serviw-params.service';
import { Categoria } from 'src/app/world/categoria';

@Component({
  selector: 'app-categoria-form-ruta',
  templateUrl: './categoria-form-ruta.component.html',
  styles: []
})
export class CategoriaFormRutaComponent implements OnInit {

  formCategoria: FormGroup;

  private categoria: Categoria

  constructor(
    private formBuilder: FormBuilder,
    private serviwParams:ServiwParams,
    private location:Location
  ) {
    // this.categoria = config ? config.data : {};
    this.categoria= serviwParams.data.categoria || {};
  }

  ngOnInit() {
    this.formCategoria = this.formBuilder.group({
      nombre: [
        this.categoria.nombre, // valor
        Validators.compose([
          Validators.required,
          Validators.maxLength(64),
          Validators.minLength(1)
        ])
      ],
      estado: [
        this.categoria.estado, // valor
        Validators.compose([
          Validators.required,
          Validators.maxLength(1)
        ])
      ],
    });
  }

  ver() {
    console.log(this.formCategoria);
  }

  cancelar(){
    this.location.back()
  }
}
