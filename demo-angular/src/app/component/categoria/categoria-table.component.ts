import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DialogService } from 'primeng/api';
import { CategoriaService } from 'src/app/service/categoria.service';
import { ServiwParams } from 'src/app/service/serviw-params.service';
import { Categoria } from 'src/app/world/categoria';
import { CategoriaFormComponent } from './categoria-form.component';

@Component({
  selector: 'app-categoria-table',
  templateUrl: './categoria-table.component.html'
})
export class CategoriaTableComponent implements OnInit {

  cateogrias: Categoria[];

  categoria: Categoria;

  cols: any[];

  constructor(
    public dialogService: DialogService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private serviwParams: ServiwParams,
    private categoriaService: CategoriaService
  ) { }

  ngOnInit() {
    this.getAll();
    // this.cateogrias = [
    //   {
    //     nombre: 'Frutas',
    //     estado: 'A',
    //     id: 1
    //   },
    //   {
    //     nombre: 'Lacteos',
    //     estado: 'I',
    //     id: 2
    //   }
    // ]

    this.cols = [
      { field: 'nombre', header: 'Nombre' },
      { field: 'estado', header: 'Estado' },
    ];
  }

  // Por dialogo dinamico

  addEdit(nuevo = true) {
    const header = nuevo ? 'Crear Categoría' : 'Editar Categoría'
    const ref = this.dialogService.open(CategoriaFormComponent, {
      header: header,
      width: '30%',
      data: nuevo ? {} : this.categoria
    });
  }

  // Por navegación

  showForm(route: string, nuevo = true) {
    this.serviwParams.data.categoria = nuevo ? {} : this.categoria;
    const params: { relativeTo: ActivatedRoute } = { relativeTo: this.activatedRoute };
    this.router.navigate([route], params)
  }

  getAll() {
    const getOk = (list: Categoria[]) => {
      this.cateogrias = list;
    }
    const getError = (error) => {
      console.log(error);
    }
    const promesa = this.categoriaService.getAll();
    promesa.then(getOk, getError)
  }
}
