import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DynamicDialogConfig, } from 'primeng/api';
import { CategoriaService } from 'src/app/service/categoria.service';
import { Categoria } from 'src/app/world/categoria';

@Component({
  selector: 'app-categoria-form',
  templateUrl: './categoria-form.component.html',
  styles: []
})
export class CategoriaFormComponent implements OnInit {

  formCategoria: FormGroup;

  private categoria: Categoria

  constructor(
    private formBuilder: FormBuilder,
    public config: DynamicDialogConfig,
    private categoriaService: CategoriaService
  ) {
    this.categoria = config ? config.data : {};
  }

  ngOnInit() {
    this.formCategoria = this.formBuilder.group({
      nombre: [
        this.categoria.nombre, // valor
        Validators.compose([
          Validators.required,
          Validators.maxLength(64),
          Validators.minLength(1)
        ])
      ],
      estado: [
        this.categoria.estado, // valor
        Validators.compose([
          Validators.required,
          Validators.maxLength(1)
        ])
      ],
    });
  }

  ver() {
    console.log(this.formCategoria);
  }

  save() {
    const saveOk = (categoria: Categoria) => {
      console.log(categoria);
      console.log('Guardado OK');
    }

    const saveError = (error) => {
      console.error(error);
    }

    const promesa = this.categoriaService.save(this.formCategoria.value);
    promesa.then(saveOk, saveError);
  }

}
