import { Component, OnInit } from '@angular/core';
import { DetalleFactura } from 'src/app/world/detalle-factura';
import { FilterUtils } from "primeng/api";

@Component({
  selector: 'app-detalle-factura-table',
  templateUrl: './detalle-factura-table.component.html',
  styles: [".background-red{background:red !important;}"]
})
export class DetalleFacturaTableComponent implements OnInit {

  detallesFactura: DetalleFactura[]

  cols: any[];

  constructor() { }

  ngOnInit() {
    this.detallesFactura = [
      {
        id: 1,
        cantidad: 10,
        fechaCompra: new Date(),
        producto: {
          id: 1,
          cantidad: 10,
          nombre: 'Leche Bolsa',
          precio: 500,
          categoria: { id: 2, nombre: 'Lacteos', estado: 'I' }
        }
      },      {
        id: 2,
        cantidad: 120,
        fechaCompra: new Date(),
        producto: {
          id: 1,
          cantidad: 120,
          nombre: 'Queso',
          precio: 5200,
          categoria: { id: 2, nombre: 'S Lacteos', estado: 'A' }
        }
      }
    ]

    this.cols = [
      { field: 'producto', header: 'Producto', filterBy: 'producto.nombre' },
      { field: 'cantidad', header: 'Cantidad', filterBy: 'cantidad' },
      { field: 'fechaCompra', header: 'Fecha Compra', filterBy: 'fechaCompra' },
      { field: 'categoria', header: 'Categoría', filterBy: 'producto.categoria.nombre' },
    ];

    // filtro personalizado
    FilterUtils['date'] = (value: Date, filter: Date): boolean => {
      return value.toLocaleDateString() == filter.toLocaleDateString();
    }
  }
}
