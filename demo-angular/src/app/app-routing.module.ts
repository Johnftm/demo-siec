import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoriaFormRutaComponent } from './component/categoria/categoria-form-ruta.component';
import { CategoriaFormComponent } from './component/categoria/categoria-form.component';
import { CategoriaTableComponent } from './component/categoria/categoria-table.component';
import { DetalleFacturaTableComponent } from './component/detalle-factura/detalle-factura-table/detalle-factura-table.component';
import { ProductoTableComponent } from './component/producto/producto-table.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'categoria',
    pathMatch: 'full'
  }, {
    // /categoria/form
    path: 'categoria',
    component: CategoriaTableComponent,
    children: [
      {
        path: 'form',
        component: CategoriaFormRutaComponent
      }
    ]
  }, {
    path: 'producto',
    component: ProductoTableComponent
  }, {
    path: 'detalle-factura',
    component: DetalleFacturaTableComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
