import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { CategoriaTableComponent } from './component/categoria/categoria-table.component';
import { HomeComponent } from './component/home/home/home.component';
import { ProductoTableComponent } from './component/producto/producto-table.component';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { InputTextModule } from 'primeng/inputtext';
import { DropdownModule } from 'primeng/dropdown';
import { DetalleFacturaTableComponent } from './component/detalle-factura/detalle-factura-table/detalle-factura-table.component';
import { CalendarModule } from 'primeng/calendar';
import { CategoriaFormComponent } from './component/categoria/categoria-form.component';
import { RadioButtonModule } from 'primeng/radiobutton';
import { MessageModule } from 'primeng/message';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import {DialogModule} from 'primeng/dialog';

import { ActivoInactivoPipe } from './pipe/activo-inactivo.pipe';
import { ReactiveFormsModule } from '@angular/forms';
import { DialogService } from 'primeng/api';
import { CategoriaFormRutaComponent } from './component/categoria/categoria-form-ruta.component';
import { HttpClientModule } from '@angular/common/http';
import { ProductoFormComponent } from './component/producto/producto-form.component';

@NgModule({
  declarations: [
    AppComponent,
    CategoriaTableComponent,
    HomeComponent,
    ProductoTableComponent,
    DetalleFacturaTableComponent,
    CategoriaFormComponent,
    ActivoInactivoPipe,
    CategoriaFormRutaComponent,
    ProductoFormComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpClientModule,
    // primeng
    ButtonModule,
    TableModule,
    InputTextModule,
    DropdownModule,
    CalendarModule,
    RadioButtonModule,
    MessageModule,
    DynamicDialogModule,
    DialogModule
  ],
  entryComponents:[
    CategoriaFormComponent,
    ProductoFormComponent
  ],
  providers: [DialogService],
  bootstrap: [AppComponent]
})
export class AppModule { }
